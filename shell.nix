{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, bifunctors, containers, stdenv, these
      , transformers, vector
      }:
      mkDerivation {
        pname = "compactable";
        version = "0.1.1.0";
        src = ./.;
        libraryHaskellDepends = [
          base bifunctors containers these transformers vector
        ];
        description = "A typeclass for structures which can be catMaybed, filtered, and partitioned";
        license = stdenv.lib.licenses.bsd3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
